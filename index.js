/*============================Bài 1 ======================= */
/* -----------------STAR NGÀY HÔM QUA ------------------------ */

var btnNgayHomQua = document.getElementById("btnngayhomqua");
btnNgayHomQua.onclick = function () {
  //   input : nhapngay,nhapthang,nhapnam : number
  var nhapNgay = Number(document.getElementById("nhapngay").value);
  var nhapThang = Number(document.getElementById("nhapthang").value);
  var nhapNam = Number(document.getElementById("nhapnam").value);
  //output ketqua:string
  var ketQua1 = "";
  //PROGRESS
  //(1) điều kiển để là 1 ngày trong tháng : ngày là số nguyên dùng hàm Number.isInteger(nhapNgay); nhapngay >0
  var dieuKienNgay = Number.isInteger(nhapNgay) && nhapNgay > 0;

  //(2) điều kiện để là 1 năm : năm nhập vào phải là số nguyên nhapnam >=0
  var dieuKienNam = Number.isInteger(nhapNam) && nhapNam >= 0;

  //(3) điều kiện để là 1 tháng 30 ngày
  var dieuKienThangBaMuoiNgay =
    (nhapThang === 4 ||
      nhapThang === 6 ||
      nhapThang === 9 ||
      nhapThang === 11) &&
    nhapNgay <= 30;

  //(4) điều kiện để là 1 tháng 31 ngày e để điều kiện ngày <= 31
  var dieuKienThangBaMotNgay =
    (nhapThang === 1 ||
      nhapThang === 3 ||
      nhapThang === 5 ||
      nhapThang === 7 ||
      nhapThang === 8 ||
      nhapThang === 10 ||
      nhapThang === 12) &&
    nhapNgay <= 31;

  //(5) điều kiện để là tháng 2 e để ngày <= 28
  var dieuKienThangHai = nhapThang === 2 && nhapNgay <= 28;

  //điều kiển để là 1 ngày tháng năm hợp lệ : Từ (1)(2)(3)(4)(5) =>>> điều để người dùng nhập vào là 1 ngày tháng năm hợp lệ
  var dieuKienNgayThangNam =
    (dieuKienThangBaMotNgay || dieuKienThangBaMuoiNgay || dieuKienThangHai) &&
    dieuKienNgay &&
    dieuKienNam;

  /*
  Tạo biến chứa điều kiện để ngày đầu tháng này về ngày cuối tháng các tháng có 31 ngày: 
  gồm:
  1/2 ->31 /1 
  1/4 -> 31/3
  1/6 -> 31 / 5
  1/8 -> 31/7
  1 /9 -> 31 / 8
  1/11 -> 31/10
  */
  var chuyenDauThangVeCuoiThangBaMotNgay =
    (nhapThang === 2 ||
      nhapThang === 4 ||
      nhapThang === 6 ||
      nhapThang === 8 ||
      nhapThang === 9 ||
      nhapThang === 11) &&
    nhapNgay === 1;

  /*
 tạo biến chứa điều kiện để ngày đầu tháng này về  ngày cuối tháng các tháng có 30 ngày
 gồm:
1/5 -> 30/4
1/7 -> 30/6
1/10 -> 30/9
1/12 -> 30/11
 */
  var chuyenDauThangVeCuoiThangBaMuoiNgay =
    (nhapThang === 5 ||
      nhapThang === 7 ||
      nhapThang === 10 ||
      nhapThang === 12) &&
    nhapNgay === 1;

  /*
Tạo biến chứa điều kiện đầu tháng 3 về cuối tháng 2 
gồm:
1/3 -> 28/2
*/
  var chuyenDauThangBaVeCuoiThangHai = nhapThang === 3 && nhapNgay === 1;

  /*
Tạo biến để chuyển từ đầu năm này về cuối năm kia 
ví dụ:
1/1/2020 -> 31/12/2019
*/
  var chuyenDauNamVeCuoiNam = nhapNgay === 1 && nhapThang === 1;

  // Bắt đầu giải nè
  if (dieuKienNgayThangNam) {
    //điều kiện chuyển năm : 1/1/0 -> 1/1/-1 vô lý
    if (chuyenDauNamVeCuoiNam && nhapNam === 0) {
      ketQua1 = " Ngày đó không có đâu bạn êyyyy!";
      //điều kiện chuyển năm  ex : 1/1/2022 -> 31/12/2021
    } else {
      if (chuyenDauNamVeCuoiNam) {
        nhapNgay = 31;
        nhapThang = 12;
        nhapNam = nhapNam - 1;
        //điều kiện chuyển ngày đầu tháng ngày về cuối ngày tháng 31 ngày
      } else if (chuyenDauThangVeCuoiThangBaMotNgay) {
        nhapNgay = 31;
        nhapThang = nhapThang - 1;
        //điều kiện chuyển ngày đầu tháng này về cuối tháng 30 ngày
      } else if (chuyenDauThangVeCuoiThangBaMuoiNgay) {
        nhapNgay = 30;
        nhapThang = nhapThang - 1;
        //điều kiện chuyển đầu tháng 3 về cuối tháng 2
      } else if (chuyenDauThangBaVeCuoiThangHai) {
        nhapNgay = 28;
        nhapThang = nhapThang - 1;
      } else {
        nhapNgay = nhapNgay - 1;
      }
      ketQua1 = ` ngày ${nhapNgay} / ${nhapThang} / ${nhapNam}`;
    }
  } else {
    ketQua1 = `Ngày nhập không hợp lệ `;
  }
  document.getElementById("ketqua1").innerHTML = ketQua1;
};

/* -----------------END NGÀY HÔM QUA ------------------------ */

/* -----------------STAR NGÀY MAI ------------------------ */
document.getElementById("btnngaymai").onclick = function () {
  //   input : nhapngay,nhapthang,nhapnam : number
  var nhapNgay = Number(document.getElementById("nhapngay").value);
  var nhapThang = Number(document.getElementById("nhapthang").value);
  var nhapNam = Number(document.getElementById("nhapnam").value);
  //output ketqua1:string
  var ketQua1 = "";
  //PROGRESS
  //(1) điều kiển để là 1 ngày trong tháng : ngày là số nguyên dùng hàm Number.isInteger(nhapNgay); nhapngay >0
  var dieuKienNgay = Number.isInteger(nhapNgay) && nhapNgay > 0;

  //(2) điều kiện để là 1 năm : năm nhập vào phải là số nguyên nhapnam >=0
  var dieuKienNam = Number.isInteger(nhapNam) && nhapNam >= 0;

  //(3) điều kiện để là 1 tháng 30 ngày
  var dieuKienThangBaMuoiNgay =
    (nhapThang === 4 ||
      nhapThang === 6 ||
      nhapThang === 9 ||
      nhapThang === 11) &&
    nhapNgay <= 30;

  //(4) điều kiện để là 1 tháng 31 ngày e để điều kiện ngày <= 31
  var dieuKienThangBaMotNgay =
    (nhapThang === 1 ||
      nhapThang === 3 ||
      nhapThang === 5 ||
      nhapThang === 7 ||
      nhapThang === 8 ||
      nhapThang === 10 ||
      nhapThang === 12) &&
    nhapNgay <= 31;

  //(5) điều kiện để là tháng 2 e để ngày <= 28
  var dieuKienThangHai = nhapThang === 2 && nhapNgay <= 28;

  //điều kiển để là 1 ngày tháng năm hợp lệ : Từ (1)(2)(3)(4)(5) =>>> điều để người dùng nhập vào là 1 ngày tháng năm hợp lệ
  var dieuKienNgayThangNam =
    (dieuKienThangBaMotNgay || dieuKienThangBaMuoiNgay || dieuKienThangHai) &&
    dieuKienNgay &&
    dieuKienNam;
  /*
tạo biến chưa điều kiện cuối năm này về đầu năm kia
ví dụ: 
31/12 -> 1/1
 */
  var chuyenCuoiNamVeDauNam = nhapNgay === 31 && nhapThang === 12;

  /*
tạo biến chứa điều kiện chuyển cuối tháng 30 ngày về đầu tháng sau 
gồm: 
30/4 -> 1/5
30/6 -> 1/7
30/9 -> 1/10
30/11 -> 1/12
 */
  var chuyenCuoiThangBaMuoiNgayVeDauThang =
    (nhapThang === 4 ||
      nhapThang === 6 ||
      nhapThang === 9 ||
      nhapThang === 11) &&
    nhapNgay === 30;
  /*
tạo biến chứa điều kiện chuyển cuối tháng 31 ngày về đầu tháng sau
gồm: trừ tháng 12 vì 31/12 -> 1/1 là chuyển từ năm này sang năm khác
31/1 -> 1/2
31/3 -> 1/4
31/5 -> 1/6
31/7 -> 1/8
31/8 -> 1/9
31/10 -> 1/11
*/
  var chuyenCuoiThangBaMotNgayVeDauThang =
    (nhapThang === 1 ||
      nhapThang === 3 ||
      nhapThang === 5 ||
      nhapThang === 7 ||
      nhapThang === 8 ||
      nhapThang === 10) &&
    nhapNgay === 31;
  /*
tạo biến chứa điều kiện chuyển cuối tháng 2  về đầu tháng 3
gồm: 28/2 -> 1/3
*/
  var chuyenCuoiThangHaiVeDauThangBa = nhapThang === 2 && nhapNgay === 28;

  // bắt đầu giải nào
  if (dieuKienNgayThangNam) {
    //điều kiện chuyển cuối năm này về đầu năm kia : 31/12/2000 -> 1/1/2001
    if (chuyenCuoiNamVeDauNam) {
      nhapNgay = 1;
      nhapThang = 1;
      nhapNam = nhapNam + 1;
      //điều kiện chuyển cuối tháng 30 ngày về đầu tháng sau : 30/4 -> 1/5
    } else if (chuyenCuoiThangBaMuoiNgayVeDauThang) {
      nhapNgay = 1;
      nhapThang = nhapThang + 1;
      //điều kiện chuyển cuối tháng 31 ngày về đầu tháng sau : 31/1 -> 1/2
    } else if (chuyenCuoiThangBaMotNgayVeDauThang) {
      nhapNgay = 1;
      nhapThang = nhapThang + 1;
      //điều kiện chuyển cuối tháng 2 về đầu tháng 3: 28/2 -> 1/3
    } else if (chuyenCuoiThangHaiVeDauThangBa) {
      nhapNgay = 1;
      nhapThang = nhapThang + 1;
    } else {
      nhapNgay = nhapNgay + 1;
    }
    ketQua1 = `Ngày ${nhapNgay} / ${nhapThang} / ${nhapNam}`;
  } else {
    ketQua1 = "Ngày Nhập không hợp lệ";
  }
  document.getElementById("ketqua1").innerHTML = ketQua1;
};
/*---------------------END NGÀY MAI------------------------ */

/*==========================BÀI 2======================== */
document.getElementById("btntinhngay").onclick = function () {
  var nhapThang2 = Number(document.getElementById("nhapthang2").value);
  var nhapNam2 = Number(document.getElementById("nhapnam2").value);
  //output ketqua:string
  var ketQua2 = "";
  //(1) điều kiện để là 1 năm : năm nhập vào phải là số nguyên nhapnam2 >=0
  var dieuKienNam = Number.isInteger(nhapNam2) && nhapNam2 >= 0;
  //(2) điều kiện nhập vào là 1 tháng hợp lệ
  var dieuKienThang =
    Number.isInteger(nhapThang2) && nhapThang2 >= 0 && nhapThang2 <= 12;
  // điều kiện để nhập vào tháng năm là hợp lệ
  var dieuKienThangNam = dieuKienNam && dieuKienThang;
  //tháng 30 ngày
  var dieuKienThangBaMuoiNgay =
    nhapThang2 === 4 ||
    nhapThang2 === 6 ||
    nhapThang2 === 9 ||
    nhapThang2 === 11;
  // tháng 2
  var dieuKienThangHai = nhapThang2 === 2;
  // tháng 31 ngày là các tháng còn lại
  // điều kiện năm nhuận
  var NamNhuan = nhapNam2 % 4 === 0;

  if (dieuKienThangNam) {
    // năm nhuận thì tháng 2 có 29 ngày
    if (NamNhuan && dieuKienThangHai) {
      ketQua2 = `tháng ${nhapThang2} năm ${nhapNam2} có 29 ngày `;
      //năm bình thường thì tháng 2 có 28 ngày
    } else if (dieuKienThangHai) {
      ketQua2 = `tháng ${nhapThang2} năm ${nhapNam2} có 28 ngày `;
    } else if (dieuKienThangBaMuoiNgay) {
      ketQua2 = `tháng ${nhapThang2} năm ${nhapNam2} có 30 ngày `;
    } else {
      ketQua2 = `tháng ${nhapThang2} năm ${nhapNam2} có 31 ngày `;
    }
  } else {
    ketQua2 = "Nhập không hợp lệ";
  }
  document.getElementById("ketqua2").innerHTML = ketQua2;
};
/*============================BÀI 3======================= */
document.getElementById("btncachdoc").onclick = function () {
  //   input nhapso : number
  var nhapSo = document.getElementById("nhapso").value * 1;
  //output ketqua: string
  var cachDocHangTram = "";
  var cachDocHangChuc = "";
  var cachDocHangDonVi = "";
  var ketQua3 = "";
  //progress
  //điều kiện để là 1 số nguyên
  var dieuKienLaSoNguyen = Number.isInteger(nhapSo);

  //lấy các số hàng trăm, hàng chục, hàng đơn vị
  var hangDonVi = (nhapSo % 10) * 1;
  var hangChuc = (((nhapSo % 100) - hangDonVi) / 10) * 1;
  // var hangTram = (nhapSo / 100 - (nhapSo % 100) / 100) * 1;
  var hangTram = Math.floor(nhapSo / 100) * 1;

  // nhập số nguyên
  if (dieuKienLaSoNguyen) {
    if (nhapSo >= -9 && nhapSo <= 9) {
      alert("Nhập thêm 2 số nữa đi b êyyy");
    } else if ((nhapSo > 9 && nhapSo <= 99) || (nhapSo >= -99 && nhapSo < -9)) {
      alert("nhập thêm 1 số nữa b êyyy");
    } else if (nhapSo < -999 || nhapSo > 999) {
      alert("nhập dư số rồi b êyyyy");
    } else {
      //cách đọc hàng trăm số nguyên dương
      if (hangTram === 1) {
        cachDocHangTram = "Một trăm";
      } else if (hangTram === 2) {
        cachDocHangTram = "Hai trăm";
      } else if (hangTram === 3) {
        cachDocHangTram = "Ba trăm";
      } else if (hangTram === 4) {
        cachDocHangTram = "Bốn trăm";
      } else if (hangTram === 5) {
        cachDocHangTram = "Năm trăm";
      } else if (hangTram === 6) {
        cachDocHangTram = "Sáu trăm";
      } else if (hangTram === 7) {
        cachDocHangTram = "Bảy trăm";
      } else if (hangTram === 8) {
        cachDocHangTram = "Tám trăm";
      } else if (hangTram === 9) {
        cachDocHangTram = "Chín trăm";
      }
      //cách đọc hàng trăm số nguyên âm
      if (hangTram === -1) {
        cachDocHangTram = "Âm Một trăm";
      } else if (hangTram === -2) {
        cachDocHangTram = "Âm Hai trăm";
      } else if (hangTram === -3) {
        cachDocHangTram = "Âm Ba trăm";
      } else if (hangTram === -4) {
        cachDocHangTram = "Âm Bốn trăm";
      } else if (hangTram === -5) {
        cachDocHangTram = "Âm Năm trăm";
      } else if (hangTram === -6) {
        cachDocHangTram = "Âm Sáu trăm";
      } else if (hangTram === -7) {
        cachDocHangTram = "Âm Bảy trăm";
      } else if (hangTram === -8) {
        cachDocHangTram = "Âm Tám trăm";
      } else if (hangTram === -9) {
        cachDocHangTram = "Âm Chín trăm";
      }
      //cách đọc hàng chục
      if (hangChuc === 1 || hangChuc === -1) {
        cachDocHangChuc = "mười";
      } else if (hangChuc === 2 || hangChuc === -2) {
        cachDocHangChuc = "Hai mươi";
      } else if (hangChuc === 3 || hangChuc === -3) {
        cachDocHangChuc = "Ba mươi";
      } else if (hangChuc === 4 || hangChuc === -4) {
        cachDocHangChuc = "Bốn mươi";
      } else if (hangChuc === 5 || hangChuc === -5) {
        cachDocHangChuc = "Năm mươi";
      } else if (hangChuc === 6 || hangChuc === -6) {
        cachDocHangChuc = "Sáu mươi";
      } else if (hangChuc === 7 || hangChuc === -7) {
        cachDocHangChuc = "Bảy mươi";
      } else if (hangChuc === 8 || hangChuc === -8) {
        cachDocHangChuc = "Tám mươi";
      } else if (hangChuc === 9 || hangChuc === -9) {
        cachDocHangChuc = "Chín mươi";
      } else if (hangChuc === 0 && hangDonVi === 0) {
        cachDocHangChuc = "";
      } else if (hangChuc === 0) {
        cachDocHangChuc = "lẻ";
      }
      if (hangDonVi === 1 || hangDonVi === -1) {
        //cách đọc hàng đơn vị
        cachDocHangDonVi = "một";
      } else if (hangDonVi === 2 || hangDonVi === -2) {
        cachDocHangDonVi = "Hai";
      } else if (hangDonVi === 3 || hangDonVi === -3) {
        cachDocHangDonVi = "Ba";
      } else if (hangDonVi === 4 || hangDonVi === -4) {
        cachDocHangDonVi = "Bốn";
      } else if (hangDonVi === 5 || hangDonVi === -5) {
        cachDocHangDonVi = "lăm";
      } else if (hangDonVi === 6 || hangDonVi === -6) {
        cachDocHangDonVi = "Sáu";
      } else if (hangDonVi === 7 || hangDonVi === -7) {
        cachDocHangDonVi = "Bảy";
      } else if (hangDonVi === 8 || hangDonVi === -8) {
        cachDocHangDonVi = "Tám";
      } else if (hangDonVi === 9 || hangDonVi === -9) {
        cachDocHangDonVi = "Chín";
      } else {
        cachDocHangDonVi = "";
      }

      ketQua3 = `${cachDocHangTram} ${cachDocHangChuc} ${cachDocHangDonVi}`;
    }
  } else {
    ketQua3 = "nhập số không hợp lệ";
  }
  document.getElementById("ketqua3").innerHTML = ketQua3;
};
/*============================BÀI 4======================== */
document.getElementById("btntimdiachi").onclick = function () {
  event.preventDefault();
  /*
input:
tensinhvien1,tensinhvien2,tensinhvien3 : string
toadoxsinhvien1,toadoxsinhvien2,toadoxsinhvien3 :number
toadoysinhvien1,toadoysinhvien2,toadoysinhvien3: number
*/
  //tên sinh viên
  var tenSVMot = document.getElementById("tensinhvienmot").value;
  var tenSVHai = document.getElementById("tensinhvienhai").value;
  var tenSVBa = document.getElementById("tensinhvienba").value;
  // tọa độ x
  var toaDoXSV1 = document.getElementById("toadoxsinhvienmot").value * 1;
  var toaDoXSV2 = document.getElementById("toadoxsinhvienhai").value * 1;
  var toaDoXSV3 = document.getElementById("toadoxsinhvienba").value * 1;
  var toaDoXTH = document.getElementById("toadoxtruonghoc").value * 1;
  //tọa độ Y
  var toaDoYSV1 = document.getElementById("toadoysinhvienmot").value * 1;
  var toaDoYSV2 = document.getElementById("toadoysinhvienhai").value * 1;
  var toaDoYSV3 = document.getElementById("toadoysinhvienba").value * 1;
  var toaDoYTH = document.getElementById("toadoytruonghoc").value * 1;
  //output: ketqua: string
  var ketQua4 = "";
  //progress
  //Khoảng cách từ nhà Sinh viên 1 đến trường
  var dSV1 = Math.sqrt(
    Math.pow(toaDoXTH - toaDoXSV1, 2) + Math.pow(toaDoYTH - toaDoYSV1, 2)
  );
  console.log("dSV1: ", dSV1);
  //Khoảng cách từ nhà Sinh viên 2 đến trường
  var dSV2 = Math.sqrt(
    Math.pow(toaDoXTH - toaDoXSV2, 2) + Math.pow(toaDoYTH - toaDoYSV2, 2)
  );
  console.log("dSV2: ", dSV2);
  //khoảng cách từ nhà Sinh viên 3 đến trường
  var dSV3 = Math.sqrt(
    Math.pow(toaDoXTH - toaDoXSV3, 2) + Math.pow(toaDoYTH - toaDoYSV3, 2)
  );
  console.log("dSV3: ", dSV3);
  // nhà sinh viên 1 xa nhất
  if (dSV1 > dSV2 && dSV1 > dSV3) {
    ketQua4 = `Nhà bạn ${tenSVMot} xa nhất `;
    // nhà sinh viên 2 xa nhất
  } else if (dSV2 > dSV1 && dSV2 > dSV3) {
    ketQua4 = `Nhà bạn ${tenSVHai} xa nhất `;
    //nhà sinh viên 3 xa nhất
  } else if (dSV3 > dSV1 && dSV3 > dSV2) {
    ketQua4 = `Nhà bạn ${tenSVBa} xa nhất `;
    //khoảng cách từ nhà đên trường của sv1 và sv2 bằng nhau và xa nhất
  } else if (dSV1 === dSV2 && dSV1 > dSV3) {
    ketQua4 = `Khoảng cách tới trường của nhà bạn ${tenSVMot}, bạn ${tenSVHai} là bằng nhau và xa trường nhất `;
    //khoảng cách từ nhà đên trường của sv1 và sv3 bằng nhau và xa nhất
  } else if (dSV1 === dSV3 && dSV1 > dSV2) {
    ketQua4 = `Khoảng cách tới trường của nhà bạn ${tenSVMot}, bạn ${tenSVBa} là bằng nhau và xa trường nhất `;
    //khoảng cách từ nhà đên trường của sv2 và sv3 bằng nhau và xa nhất
  } else if (dSV2 === dSV3 && dSV2 > dSV1) {
    ketQua4 = `Khoảng cách tới trường của nhà bạn ${tenSVHai}, bạn ${tenSVBa} là bằng nhau và xa trường nhất `;
  } else if (dSV1 === dSV2 && dSV1 === dSV3) {
    ketQua4 = "khoảng cách từ nhà đến trường của 3 bạn là bằng nhau";
  } else {
    ketQua4 = "chưa nghĩ ra";
  }
  document.getElementById("ketqua4").innerHTML = ketQua4;
};
